app.controller('loginController',function($ionicLoading,$scope,$stateParams,$state,$ionicModal,$ionicPopup,$ionicPlatform, $rootScope,$http,localStorageService,$cordovaAppVersion){
  
  // $scope.pwd = "nikhil@1";
  // $scope.emailId = "ssunikhilbhilare@gmail.com";
  $scope.formData = {};
  $scope.formData.registeredemailId = '';
  $scope.formData.agencyName = '';
  // $scope.pwd = "kiran@11";
  // $scope.emailId = "ssukiranpawar@gmail.com";
  console.log($scope.formData.agencyName);
   $scope.LogIn = function(){
     console.log($scope.emailId, $scope.pwd)
     console.log($scope.formData.agencyName);
     

    $scope.userCred = {
      "email_id": $scope.emailId,
      "password": $scope.pwd
       }
    console.log($scope.userCred)
    $http({
      method: 'POST',
      url: MSMARTEU.baseURL + MSMARTEU.validateUser,
      headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": "login" ,"userId" : "null"},
      data: $scope.userCred,
    })
   .then(function(response) {
      console.log("login_Response")
      console.log(response)
      if (response.data.status == "Authorized") {
            // Setting Local Storage
             localStorageService.set("username", response.data.value[0].username)
             localStorageService.set("email", response.data.value[0].email_id)
             localStorageService.set("user_id", response.data.value[0].user_id)
             localStorageService.set("user_role", response.data.value[0].user_role)
             localStorageService.set("agency_id", response.data.value[0].agency_id)
             localStorageService.set("token", response.data.token)
            // Getting Local Storage
            console.log(localStorageService.get("token"));
            console.log(localStorageService.get("user_id"));
            if(localStorageService.get("user_role") == "agency_mr")
            {
               $state.go('app.dashboard')
            }
            else
            {
              alert("Admin doesn't have access to login")
            }
          }
          else if(response.data.status == "Unauthorized")
          {
            alert("Please Check Your Login Credentials")
          }
        },
        function(err) { // optional
           console.log(err)
          // console.error(err)
           if(err.data.status == "Unauthorized")
          {
            alert("Please Check Your Login Credentials")
          }
       })
}
  $ionicModal.fromTemplateUrl('modal.html', {
    scope: $scope
    // animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
    $scope.formData.registeredemailId = '';

      $scope.showAlert = function(loginformdata) {
        
        $ionicLoading.show({
        template: 'Sending The Mail<ion-spinner></ion-spinner>',
         animation: 'fade-in',
          noBackdrop: false,
          maxWidth: 200,
          showDelay: 500
      }).then(function() {
        console.log("The loading indicator is now displayed");
      });

   console.log($scope.formData.registeredemailId)
    $scope.userEmail = {
      "email_id": $scope.formData.registeredemailId
       }
    console.log($scope.userEmail)
    $http({
      method: 'POST',
      url: MSMARTEU.baseURL + "/getforgetpasswordemailvaliadtion",
      headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": "login" ,"userId" : "null"},
      data: $scope.userEmail,
    })
   .then(function(response) {
      console.log("Forgot Password Response")
      console.log(response)
      console.log(response.data.status)
      if(response.data.success ==  true)
      {
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
    title : 'Successfully Sent The Reset Link',
    template: 'The Password Reset Link has been sent to the registered e-mail id, Reset the Password and Login with new Password'
  });
   alertPopup.then(function(res) {
     console.log('Test  Alert');
     $scope.modal.hide();
     //$state.go('PasswordLink');
   });
      }
      
    })
   .catch(function(e){
    // handle errors in processing or in error.
    console.log(e)
    console.log(e.data.success)
    if(e.data.success == false)
    {
      $ionicLoading.hide();
     // alert("Your Email-Id is not Authroised to receive the reset Password Link")
      var alertPopup = $ionicPopup.alert({
    title : 'Cannot Sent',
    template: 'Your Email-Id is not Authorized to receive the reset Password Link'
  });
   alertPopup.then(function(res) {
     console.log('Test  Alert');
     $scope.modal.hide();
     //$state.go('PasswordLink');
   });
    }
});
   
  };
});


 $scope.showAlertForResetPwd = function() {
   var alertPopup = $ionicPopup.alert({
    title : 'Password Updated',
    template: 'Login With your new Password'
  });
   alertPopup.then(function(res) {
    console.log('Thank you for registering new Password');
     $scope.modal.hide();
     $state.go('UserLogin');
    });
 };

  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $ionicPlatform.registerBackButtonAction(function (event) {
    console.log("current State")
    console.log($state.current.name)
    if($state.current.name == 'UserLogin'){
      var confirmPopup = $ionicPopup.confirm({
        title: 'Exit?',
        template: 'Are you sure you want to Exit The App?'
      });
      confirmPopup.then(function(res) {
        if(res) {
          localStorage.clear();
          navigator.app.exitApp();
        } else {
          console.log('You are not sure');
          localStorage.clear();
        }
      });
    }
    else
    {
      navigator.app.backHistory();
    }
  }, 100);

})