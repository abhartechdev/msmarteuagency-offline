  app.controller('ReceiptCtrl',function($ionicLoading,$state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope,localStorageService){
    $scope.searchBy = '$'
    $scope.query = {}
    $scope.receiptData = {};
    $scope.receiptData.remarks="";
    $scope.receiptData.amountPaid = "";
    $scope.formData = {};
    $ionicLoading.show({
      template: 'Fetching Bill Details..',
      duration: 3000
    }).then(function() {
      console.log("The loading indicator is now displayed");
    });
    console.log("Receipt History")
    console.log($stateParams.receiptId)

  console.log(localStorageService.get("token"));
  $scope.getReceiptDetails = function(){
   $http({
    method: 'GET',
    url: MSMARTEU.baseURL + "/getAllReceipt/consumer/"+$scope.formData.consumerId,
          
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
        })

   .then(function(response) {
      console.log(response.data);
       if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
   else if(response.data.success == true)
     {
      console.log(response.data.data);
      $scope.ReceiptData = response.data.data;
      $ionicLoading.hide().then(function() {
        console.log("The loading indicator is now hidden");
      });
    }
    else if (response.data.success == false)
    {
      alert("No Receipts Has Been Generated")
    }

  });
  }

  $http({
    method: 'GET',
    url: MSMARTEU.baseURL + "/getAllReceipts/agency/"+localStorageService.get("agency_id"),
          //+$stateParams.custId,
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id") }
        })

  .then(function(response) {
    console.log(response.data);
    if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
    else if(response.data.success == true)
   {
    console.log(response.data.data);
    $scope.ReceiptData = response.data.data;
    $ionicLoading.hide().then(function() {
      console.log("The loading indicator is now hidden");
    });
  }
  else if (response.data.success == false)
  {
    alert("No Receipts Has Been Generated Yet")
  }

  });
  $scope.goToPaymentDetails = function()
  {

   $state.go('app.PaymentDetails');
  }

  $scope.date_rdv = $filter('date')(Date.now(), 'yyyy-MM-dd');
  $scope.goToAddDetails = function()
  {
    $state.go('app.AddReceiptDetails');
  }
  $scope.goToEditDetails = function()
  {
   $scope.editEmail = true;
   $state.go('app.EditReceiptDetails');
  }
  $scope.saveReceiptDetails = function(data)
  {
    $scope.ReceiptDetails = {
      "total_amount" : data.total_amount,
      "amount_paid" : $scope.receiptData.amountPaid,
      "balance_amount" : data.balance_amount,
      "payment_status" : data.payment_status,
      "remarks" : $scope.receiptData.remarks,
      "created_by" : data.created_by
    }

    console.log($scope.ReceiptDetails)
    console.log(data.balance_amount)
    if(!$scope.receiptData.remarks)
    {
      alert("The Remarks cannot be empty");
    }
    else if(data.balance_amount > $scope.receiptData.amountPaid)
    {
      alert("you cannot pay more than your balance Amount")
    }
    else if($scope.receiptData.amountPaid && data.balance_amount == 0)
    {
      alert("You Don't have to pay any money")
    }
    else
    {
      $http({
        method: 'PUT',
        url: MSMARTEU.baseURL + "/updateReceipt/"+$stateParams.receiptId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token") ,"userId" : localStorageService.get("user_id")},
        data: $scope.ReceiptDetails,
      })

      .then(function(response) {
        console.log(response)
        if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
        else if(response.data.success == true) {
          console.log("Sam console")
          $state.go('app.ReceiptList')
        }
      });
    }
  }

  $ionicPlatform.onHardwareBackButton(function() {
    console.log(' Customer Bill Details')
    console.log($state.current.name)
    if($state.current.name == 'app.ReceiptList')
    {
      $state.go('app.dashboard')
    }
  })

  });