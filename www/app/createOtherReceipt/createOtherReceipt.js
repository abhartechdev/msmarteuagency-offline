app.controller('createReceiptCtrl',function($state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$rootScope,localStorageService){
$scope.formData = {};
var customerNoArray = [];
 $scope.formData.remarks = "";
$scope.payment_status = "Partial Payment";
$scope.balance =0;

console.log(localStorageService.get("token"));
$scope.gotoCreateOtherReceipts = function()
{
 $state.go('app.CreateOtherReceipts');
}

$scope.goToEditDetails = function()
{
  $scope.editEmail = true;
  $state.go('app.EditReceiptDetails');
}
 
 
 $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getPaymentCategories/utilityBoard/1",
        //+$stateParams.custId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })
.then(function(response) {
           console.log("paymentCategory Details")
           if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
         else if(response.data.success == true)
          {
            console.log(response.data.data);
            $scope.paymentCategory = response.data.data;
          }
       });

 $scope.change = function () {
    if ($scope.balance == 0) {
            $scope.payment_status = "Full Payment";
        } else {
          console.log("else loop")
           $scope.payment_status = "Partial Payment";
        }
  
      };
$scope.goToReceiptDetails = function()
{
 customerNoArray = [];
  $scope.balance = $scope.formData.totalAmount - $scope.formData.amountPaying;
  // console.log($scope.formData.balance)
  // console.log($scope.balance)
  // console.log($scope.formData.payment_status)
  // console.log($scope.payment_status)
  // console.log($scope.formData.consumerNo);
if($scope.formData.amountPaying > $scope.formData.totalAmount)
{
  alert("Amount Paying cannot be more than totalAmount")
}
else if($scope.formData.amountPaying<=0 || $scope.formData.totalAmount<=0)
{
 alert("Amount paying and Total Amount should be greater than 0")
}
else
{
  if ($scope.balance == 0) {
            $scope.payment_status = "Full Payment";
        } else {
          console.log("else loop")
            $scope.payment_status = "Partial Payment";
        }

 $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllConsumerBillDetails/agency/"+localStorageService.get("agency_id"),
        //+$stateParams.custId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token") }
      })
.then(function(response) {
           console.log("paymentCategory Details")
            if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
         else if(response.data.success == true)
          {
           
            $scope.paymentCategory = response.data.data;
            $scope.consumerAccount = response.data.data.consumer_account_number;
            console.log($scope.paymentCategory.length)
            for(var i = 0; i< $scope.paymentCategory.length ; i++)
            {
             
             customerNoArray.push($scope.paymentCategory[i].consumer_account_number)
            }
             console.log(customerNoArray)
              console.log(customerNoArray.length)
              if(customerNoArray.includes($scope.formData.consumerNo))
              {
                console.log("Its working")
                console.log($scope.formData.paymentCategoryID)

                $scope.ReceiptDetails = 
                {
                   "utility_board_id": 1,
                   "agency_id"   : localStorageService.get("agency_id"),
                   "user_id"    : localStorageService.get("user_id"),
                   "consumer_account_number" : $scope.formData.consumerNo,
                   "payment_category_id" : $scope.formData.paymentCategoryID,
                   "total_amount" : $scope.formData.totalAmount,
                   "payment_mode" : "cash",
                   "amount_paid" : $scope.formData.amountPaying,
                   "balance_amount" : $scope.balance,
                   "payment_status" : $scope.payment_status,
                   "balance_amount" : $scope.balance,
                   "remarks" : $scope.formData.remarks,
                   "created_by" : localStorageService.get("user_id")
                }
                console.log($scope.ReceiptDetails)

                $http({
                method: 'POST',
                url: MSMARTEU.baseURL + MSMARTEU.createReceipt,
                headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token") },
                data: $scope.ReceiptDetails,
                })

           .then(function(response) {
            console.log(response)
            if(response.data.success == true) {
            $state.go('app.ReceiptList')
             }
           });
              }
              else
              { 
                console.log($scope.formData.paymentCategoryID)
                console.log("Its not working")
                alert("Please Enter Correct Customer Number")
              }
          }
       });
      
}
  
}
});