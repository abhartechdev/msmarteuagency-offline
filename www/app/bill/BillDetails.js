  app.controller('CustomerDetailsCtrl',function($ionicLoading,$state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope,localStorageService,$cordovaToast){
   
   $ionicLoading.show({
      template: 'Fetching Bill Details..',
      duration: 3000
    }).then(function() {
      console.log("The loading indicator is now displayed");
    });
   //  console.log($stateParams.custId);
   $scope.searchBy = '$';
    $scope.query = {};
    //$scope.users = userRepository.getAllUsers();

    console.log("user Data")
    console.log($scope.users)
    //console.log($scope.users.status)

     console.log(localStorageService.get("token"));
     $scope.tokenvalue = localStorageService.get("token");
     console.log(localStorageService.get("agency_id"))
    $http({
          method: 'GET',
          url: MSMARTEU.baseURL + "/getAllConsumerBillDetails/agency/"+localStorageService.get("agency_id"),
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"Content-Type" : "application/json","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
        })
  .then(function(response) {
            console.log(response.data);
            console.log(response.data.success)
               if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
                else if(response.data.success == true)
                {
                  console.log(response.data.data);
                  console.log(response.data.bill_detail_id)
                  $scope.BillData = response.data.data;
             $scope.billId = response.data.bill_detail_id;
             $ionicLoading.hide().then(function() {
        console.log("The loading indicator is now hidden");
      });
                }
              
           });

  $scope.formData = {};
  $scope.formData.customerId = "";
  console.log($stateParams.billId)
    if ($stateParams.billId) { 
      $scope.customerId = $stateParams.billId
    }
  console.log($scope.customerId)
  $scope.getBillDetails = function(customerId)
  {

    console.log($scope.customerId)
    console.log($scope.formData.customerId)
    //$cordovaToast.showShortBottom("You cannot attach more than 3 Documents")

   $http({
          method: 'GET',
          url: MSMARTEU.baseURL + "/getOneConsumerBillDetails/agency/"+localStorageService.get("agency_id")+"/consumer/"+$scope.formData.customerId,
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"Content-Type" : "application/json","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
        })
  .then(function(response) {
            console.log(response.data);
            if(response.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
             else if(response.data.success == true)
            {
            $scope.BillData = response.data.data;
            $scope.billId = response.data.bill_detail_id;
            $ionicLoading.hide().then(function() {
            console.log("The loading indicator is now hidden");
            });
            }
          else if (response.data.success == false)
                {
                  alert("Please Enter Correct Consumer Account Number")
                }
        });
  }
      
      $scope.goToBillDetails = function()
      {
        $state.go('app.CustomerBillDetails');
      }

      $scope.goToAddDetails = function()
      {
        $state.go('app.AddReceiptDetails');
      }
      
    $scope.editEmail = false;

    $scope.goToEditDetails = function()
    {
       $scope.editEmail = true;
       $state.go('app.EditReceiptDetails');
    }

    $scope.goToPaymentDetails = function()
      {
        $state.go('app.PaymentDetails');
      }

      $scope.date_rdv = $filter('date')(Date.now(), 'yyyy-MM-dd');

      $ionicPlatform.onHardwareBackButton(function() {
        console.log(' Customer Bill Details')
        console.log($state.current.name)
        if($state.current.name == 'app.CustomerDetails')
        {
          $state.go('app.dashboard')
        }
      })

  });