  app.controller('DetailsCtrl',function($state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope,localStorageService,$cordovaToast){
      $scope.formData = {};
     // $scope.formData.amount_paying=0;
      $scope.formData.remarks = "";
      $scope.toBePaid = 0;
      $scope.paymentStatus = "";
      var tmp;
   console.log("Cust History")
    $scope.billId = $stateParams.billId;
    console.log($stateParams.billId)


    if($stateParams.custId)
    {
      $scope.custId = $stateParams.custId;
    }
    $http({
          method: 'GET',
          url: MSMARTEU.baseURL + "/getOneConsumerBillDetails/agency/"+localStorageService.get("agency_id")+"/bill/"+$stateParams.billId,
          //+$stateParams.custId,
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token") ,"userId" : localStorageService.get("user_id")}
        })
  .then(function(response) {
          console.log("CUst Details")
          console.log(response.data);
          if(response.status == 401){
           alert("Unauthorized Access")
         localStorageService.clear();
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache().then(function(){
          $state.go('UserLogin', { reload: true })
        })
        }
          else if(response.data.success == true)
            {
              console.log(response.data.data);
               $scope.BillData = response.data.data;
               tmp=response.data.data;
               onServerResponse();
            }
         });
     
  function onServerResponse() {
    $scope.date_rdv = $filter('date')(Date.now(), 'yyyy-MM-dd');
  //console.log("current Date");
  console.log($scope.date_rdv);
  var d1 = new Date($scope.BillData[0].due_date);
  var d2 = new Date($scope.date_rdv);
  console.log(d1 > d2);
  console.log($scope.BillData[0].bill_modified)
  if($scope.BillData[0].bill_modified == 'false')
  {
     if(d1 >= d2)
     {
      $scope.amountPayable = $scope.BillData[0].total_bill;
     }
     else
     {
      $scope.amountPayable = $scope.BillData[0].bill_after_due_date;
     }
  }
  else if ($scope.BillData[0].bill_modified == 'true')
  {
    $scope.amountPayable = $scope.BillData[0].revised_total_bill;
  }
     console.log("amountPayable")
     console.log($scope.formData.amount_paying)

  var split = $scope.BillData[0].due_date.split('T')
  $scope.due_date = split[0];
  console.log(split[0]);

  $scope.gotoCustomerDetails = function(data){
   if(!$scope.formData.amount_paying)
   {
     alert("Amount Paid Cannot Be Zero Or Less Than Zero")
   }
  else if($scope.formData.amount_paying > $scope.amountPayable)
  {
    alert("Amount Should Not Be Greater Than Amount Payable")
  }
  else
  {
    $scope.balance = $scope.amountPayable - $scope.formData.amount_paying;
    console.log($scope.balance)
    console.log($scope.formData.amount_paying)
    console.log($scope.amountPayable)
       $scope.$watch('balance', function () {
          if ($scope.balance == 0) {
              $scope.paymentStatus = "Full Payment";
          } else {
            console.log("else loop")
             $scope.paymentStatus = "Partial Payment";
          }
      });
        if ($scope.balance == 0) {
              $scope.paymentStatus = "Full Payment";
          } else {
            console.log("else loop")
              $scope.paymentStatus = "Partial Payment";
          }
      console.log($scope.paymentStatus)
      console.log("Aount Paying")
      console.log($scope.formData.amount_paying.toString())
       $scope.userData = {
        "utility_board_id": 1,
        "agency_id"   : data.agency_id,
        "user_id"    : localStorageService.get("user_id"),
        "consumer_account_number" : data.consumer_account_number,
        "payment_category_id" : 566,
        "bill_id" : data.bill_id,
        "bill_date" : data.bill_created_month,
        "bill_period" : data.bill_month,
        "due_date" : data.due_date,
        "total_amount" : data.total_bill,
        "payment_mode" : "Cash",
        "amount_paid" : $scope.formData.amount_paying.toString(),
        "payment_date" : $scope.date_rdv,
        "balance_amount" : $scope.balance,
        "payment_status" : $scope.paymentStatus,
        "remarks" : $scope.formData.remarks,
        "bank_name"                   : "",
        "check_number"                : "",
        "check_date"                  : "",
        "check_stauts"                : "",
        "created_by" : localStorageService.get("username"),
        //"created_by" : "Meter_Reader",
        "consumer_sub_division_id" : data.consumer_sub_division_id
        }
        console.log("userData")
      console.log($scope.userData)
     // console.log(JSON.parse($scope.userData))
     $http({
          method: 'POST',
          url: MSMARTEU.baseURL + MSMARTEU.createReceiptByMeterReader,
          headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token") ,"userId" : localStorageService.get("user_id")},
          data: $scope.userData
        })
        .then(function(sample) { //Success callback
          console.log("why is not printing")
            console.log(sample)
            console.log(sample.data)
              if(sample.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
            else if(sample.data.success == true) {
              console.log("Sam console")
              $cordovaToast.showShortBottom("Receipt Generated Successfully")
              $state.go('app.CustomerDetails')
            }
            else if(sample.data.id == 4004)
            {
              console.log("Alert Needed")
              alert("Check the Credit Limit")
            }
          },
        function(sample) { //error call back
          //Second function handles error
          console.log("Creit Limit")
          alert("Credit Limit Exceeded, Cannot Create Receipt")
      });
    }
  }

  }
  }); 