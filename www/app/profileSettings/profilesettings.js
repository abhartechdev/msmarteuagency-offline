app.controller('ProfileSettingsCtrl', function($scope, $state,$rootScope,$http,localStorageService) {
  console.log("ProfileSettingsCtrl")
 $scope.newPwd  = "";
 $scope.rPwd = "";
  $scope.userName = localStorageService.get("userName");
  $scope.email = localStorageService.get("email");
  $scope.agencyId = localStorageService.get("agency_id");
console.log(localStorageService.get("token"));
console.log(localStorageService.get("agency_id"));
  // $scope.changePwd = function(oPwd, newPwd, rPwd) {
  //   if (oPwd == $rootScope.userDetails.password) {
  //     if (newPwd == rPwd) {
  //       $cordovaToast.showShortTop('Password updated successfully')
  //     } else {
  //       $cordovaToast.showShortTop('Password did not match')
  //     }
  //   } else {
  //     $cordovaToast.showShortTop('Please enter your valid password')
  //   }
  // }
  $scope.changePwd = function(newPwd, rPwd) {
     if (newPwd == rPwd) {
    
      $scope.userPwdData = {
      "password": newPwd,
      "created_by"   : localStorageService.get("user_id")
    }
    console.log($scope.userPwdData)
      $http({
        method: 'PUT',
        url: MSMARTEU.baseURL + "/updateUserPassword/"+localStorageService.get("user_id"),
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"Content-Type" : "application/json","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")},
        data: $scope.userPwdData
      })
     .then(function(response) {
          console.log(response.data);
          console.log(response.data.data)
           if(response.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
          else if(response.data.success == true)
          {
             alert('Password updated successfully,Login With New Password')
              $state.go('UserLogin')
          }
          });
        //alert('Password updated successfully')
       // $state.go('UserLogin')
      } 
      else {
        alert('Password did not match')
      }
  }
});
