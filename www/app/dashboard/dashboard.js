app.controller('dashboardCtrl',function( $ionicHistory,$ionicLoading,$state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope,localStorageService){
 $scope.logout = function() {
      console.log('Logout')
      var confirmPopup = $ionicPopup.confirm({
        title: 'Logout',
        template: 'Are you sure you want to logout of the application?'
      });
      confirmPopup.then(function(res) {
        if (res) {
          console.log('You are sure');
          localStorage.clear();
           $ionicHistory.clearHistory();
        $ionicHistory.clearCache().then(function(){
          $state.go('UserLogin', {},{ reload: true })
        })
        // navigator.app.exitApp();
        } else {
          console.log('You are not sure');
        }
      });
    }
 $ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name == 'app.dashboard'){
  var confirmPopup = $ionicPopup.confirm({
       title: 'Exit?',
       template: 'Are you sure you want to Exit The App?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         localStorage.clear();
      navigator.app.exitApp();
       } else {
         console.log('You are not sure');
       }
     });  
    }
    else 
   { 
     navigator.app.backHistory();
    }
  }, 100);
})