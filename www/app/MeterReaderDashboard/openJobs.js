app.controller('jobCtrl',function($ionicLoading,$state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,localStorageService,ConnectivityMonitor,$cordovaNetwork,$cordovaSQLite){
 
  function loading()
  {
    $ionicLoading.show({
    template: 'Fetching Active Jobs..',
    duration: 2000,
    animation: 'fade-in',
    noBackdrop: false,
    maxWidth: 200,
    showDelay: 500
  }).then(function() {
    console.log("The loading indicator is now displayed");
  });
  }
 console.log("Job Ctrl")
 //userRepository.getAllUsers();
 $scope.meterReaderId = '';
 $scope.meterReaderId = localStorageService.get("user_id")
 
 console.log($scope.meterReaderId)
 console.log(ConnectivityMonitor.isOnline())
  console.log($cordovaNetwork.isOnline())
  
  
  if($cordovaNetwork.isOnline() == true)
      {
        $scope.networkstatus = $cordovaNetwork.isOnline();
        loading();
        getOnlineData();
       console.log("In Online page")
      }
      else if($cordovaNetwork.isOnline() == false)
      {
         $scope.networkstatus = $cordovaNetwork.isOnline();
         loading();
         console.log($scope.meterReaderId)
        getOfflineData();
        //OnServerResponse();
        console.log("In Offline ")
      }
      function getOnlineData()
      {
        console.log("INside Online Block")
         $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllJob/agency/"+localStorageService.get("agency_id"),
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })
.then(function(response) {
          console.log(response.data.data);
          if(response.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
          else if(response.data.success == true)
          {
             $scope.jobData = response.data.data;
              $ionicLoading.hide().then(function() {
            console.log("The loading indicator is now hidden");
         });
          }
          else if(response.data.success == false)
          {
            alert("Couldn't get Job Details")
          }
          });
      }

$scope.doRefresh = function() {

  $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllJob/agency/"+localStorageService.get("agency_id"),
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })
.then(function(response) {
          console.log(response.data);
          if(response.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
          else if(response.data.success == true)
          {
             $scope.jobData = response.data.data;
            $ionicLoading.hide().then(function() {
             console.log("The loading indicator is now hidden");
          });
          }
          else if(response.data.success == false)
          {
            alert("Couldn't get Job Details")
          }
           $scope.$broadcast('scroll.refreshComplete');
          });
};

function getOfflineData()
{
  $scope.jobDataOffline = [];
  $scope.jobDataOff = '';
       var query = "select DISTINCT job_id,job_status,status,meter_reader_id FROM customer";
       console.log(query)
       // ,[$scope.meterReaderId]
       $cordovaSQLite.execute(db, query).then(function(res) {
      console.log("insertId: " + res.insertId);
      console.log(res.rows.length)

      if(res.rows.length)
      {
        for(var i=0;i<res.rows.length;i++)
        {
          $scope.jobDataOffline.push(res.rows.item(i));
        }
        $scope.jobDataOff = $scope.jobDataOffline;
         $ionicLoading.hide().then(function() {
      console.log("The loading indicator is now hidden");
    });
       console.log($scope.jobDataOffline)
       console.log($scope.jobDataOff)
       console.log($scope.jobDataOffline[0].job_id)
       //OnServerResponse();
         }
     else{
        console.log("Data not there")
      }
       //console.log($scope.allData)
    }, function (err) {
      console.error(err);
    });
}
console.log($state.current.name)
$ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name == 'app.OpenJobsList'){
    $state.go('app.dashboard')
    }
    else 
    { 
     navigator.app.backHistory();
    }
  }, 100);
})
