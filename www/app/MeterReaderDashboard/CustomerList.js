app.controller('CustomerListCtrl', function($state, $ionicPopup, $timeout, $scope, $http, $ionicPlatform, $stateParams, localStorageService, $ionicLoading,$cordovaSQLite) {

$ionicLoading.show({
    template: 'Fetching Job Details...',
    duration: 3000
  }).then(function() {
    console.log("The loading indicator is now displayed");
  });
  $scope.allData = [];
  $scope.Consumers = [];
  $scope.searchBy = '$';
  $scope.query = {};
  $scope.formData = {};
  console.log($stateParams.jobId)
  localStorageService.set("jobId",$stateParams.jobId);
  $scope.noMoreItemsAvailable = true;
  console.log("Consumer Details")
  $scope.maxLimt = 100
  $scope.InitialLimit = 0;
  $scope.remainingItems = 0;
  console.log("Online Status")
 getTotalConsumers();
 showOfflineDetails();
 function getTotalConsumers()
 {
 
   var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken = ?";
   $cordovaSQLite.execute(db, query,[$stateParams.jobId,'no']).then(function(res) {
    console.log("Get Total Consumers")
    $scope.totalConsumers = res.rows.length;
    console.log($scope.totalConsumers)
    console.log(res.rows.length)
    }, function (err) {
    console.error(err);
  });

 }
function showOfflineDetails()
{
  $scope.limit = 0;
  $scope.offset = 100;  
  var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken = ? LIMIT ?, ?";
  $cordovaSQLite.execute(db, query,[$stateParams.jobId,'no',$scope.limit,$scope.offset]).then(function(res) {
    console.log("insertId: " + res.insertId);
    console.log(res.rows.length)
    $scope.remainingConsumers = $scope.totalConsumers;
   console.log($scope.remainingConsumers)
    if(res.rows.length)
    {
      for(var i=0;i<res.rows.length;i++)
      {
        $scope.allData.push(res.rows.item(i));
      }
      console.log($scope.allData.length)
      $scope.remainingConsumers = $scope.totalConsumers - 100;
      $scope.totalConsumers = $scope.totalConsumers - 100;
      console.log($scope.remainingConsumers)
      console.log($scope.totalConsumers)
      $scope.status = $scope.allData[0].status;
      console.log($scope.allData[0].status)
      if($scope.remainingConsumers < 0)
      {
        $scope.noMoreItemsAvailable = false;
      }
    }
    else{
      console.log("Data not there")
    }
    console.log($scope.allData)
     $ionicLoading.hide().then(function() {
      console.log("The loading indicator is now hidden");
    });
  }, function (err) {
    console.error(err);
  });
}
 $scope.maxlimit = 0;
$scope.loadMoreOffline = function()
{
 
  if($scope.remainingConsumers > 0)
  { 
     $scope.offset = 100; 
     $scope.maxlimit = $scope.maxlimit+100; 
     console.log("Max Limit")
     console.log($scope.maxlimit)
     var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken = ? LIMIT ?, ?";
    $cordovaSQLite.execute(db, query,[$stateParams.jobId,'no',$scope.maxlimit,$scope.offset]).then(function(res) {
    console.log("insertId: " + res.insertId);
    console.log(res.rows.length)
    $scope.remainingConsumers = $scope.totalConsumers - 100;
    console.log($scope.remainingConsumers)
    if(res.rows.length)
    {
      for(var i=0;i<res.rows.length;i++)
      {
        $scope.allData.push(res.rows.item(i));
      }
      console.log($scope.allData.length)
      console.log($scope.allData)
      $scope.remainingConsumers = $scope.totalConsumers - 100;
      $scope.totalConsumers = $scope.totalConsumers - 100;
      console.log($scope.remainingConsumers)
      console.log($scope.totalConsumers)
      if($scope.remainingConsumers < 0)
      {
        $scope.noMoreItemsAvailable = false;
        console.log("setting No More Items")
      }
    }
    else{
      console.log("Data not there")
    }
    }, function (err) {
    console.error(err);
  });
  }
 
  $scope.$broadcast('scroll.infiniteScrollComplete');
   $scope.$on('$stateChangeSuccess', function() {
   $scope.loadMoreOffline();
   });
 }
console.log("In ConsList Ctrl");
console.log($scope.allData)
// $scope.status = $scope.allData.status;
// console.log($scope.status)
// console.log($scope.allData[0].status)

function getOnlineData()
{
  $http({
  method: 'GET',
  url: MSMARTEU.baseURL + "/getAllJobDetailsSort/agency/" + localStorageService.get("agency_id") + "/job/" + localStorageService.get("jobId") + '/limit/'+$scope.InitialLimit+'/100',
  headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": localStorageService.get("token"),"userId" : localStorageService.get("user_id") }
})
.then(function(response) {
  console.log(response.data);
   if(response.status == 401){
     alert("Unauthorized Access")
     localStorageService.clear();
      $ionicHistory.clearHistory();
      $ionicHistory.clearCache().then(function(){
     $state.go('UserLogin', { reload: true })
      })
    }
    else if(response.status.success == true )
    {
       $scope.consList = response.data.data;
  $scope.Consumers.push(response.data.data);
  console.log($scope.Consumers)
  $scope.remainingItems = response.data.ramining;

  if($scope.remainingItems < 0){
    $scope.noMoreItemsAvailable = false;
  }
  $ionicLoading.hide().then(function() {
    console.log("The loading indicator is now hidden");
  });
    }
 
})
}


$scope.search = function() {
  console.log($scope.formData.custNo);
  $scope.Consumers = [];
   $scope.allData = [];

  var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken =? AND consumer_account_number = ?";
   $cordovaSQLite.execute(db, query,[$stateParams.jobId,'no',$scope.formData.custNo]).then(function(res) {
    console.log("insertId: " + res.insertId);
    console.log(res.rows.length)
   if(res.rows.length)
    {
      for(var i=0;i<res.rows.length;i++)
      {
        $scope.allData.push(res.rows.item(i)) ;
      }
      console.log("Inside if")
    }
    else{
      console.log("Data not there")
      alert("Meter Reading Already Taken")
    }
    
  }, function (err) {
    console.error(err);
  });
  
}
$ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name == 'app.CustomerList'){
    $state.go('app.syncData',{'jobId' : $stateParams.jobId})
    }
    else 
    {  
     navigator.app.backHistory();
    }
  }, 100);


});


// /getAllJobDetailsSort/agency/:agency_id/job/:job_id/limit/:from_limit/:to_limit
