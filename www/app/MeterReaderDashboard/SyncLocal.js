app.controller('syncLocalCtrl', function($state, $ionicPopup, $timeout, $scope, $http, $ionicPlatform, $stateParams, localStorageService, $ionicLoading,$cordovaSQLite) {
  $scope.allData = [];
  $scope.Consumers = [];
  $scope.searchBy = '$';
  $scope.query = {};
  $scope.formData = {};
  console.log($stateParams.jobId)
  localStorageService.set("jobId",$stateParams.jobId);
  $scope.noMoreItemsAvailable = true;
  console.log("Consumer Details")
  $scope.maxLimt = 100
  $scope.InitialLimit = 0;
  $scope.remainingItems = 0;
  $scope.jobId = $stateParams.jobId;
  $scope.flag = false ;
 
var query = "select * FROM customer WHERE job_id = ?";
console.log(query)
$cordovaSQLite.execute(db, query,[$scope.jobId]).then(function(res) {
  console.log("insertId: " + res.insertId);
  console.log(res.rows.length)

  if(res.rows.length)
  {
    $scope.consumerLength = res.rows.length;
  }
  else{
    console.log("Data not there")
    $scope.consumerLength = res.rows.length;
  }
  console.log($scope.consumerLength)
  console.log($scope.allData)
}, function (err) {
  console.error(err);
});
$http({
  method: 'GET',
  url: MSMARTEU.baseURL + "/getAllCodes/agency/" + localStorageService.get("agency_id"),
  headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": localStorageService.get("token"),"userId" : localStorageService.get("user_id") }
})
.then(function(response) {
  console.log(response.data);
  if(response.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
              else if(response.data.success == true)
              {
                $scope.errorCodes = response.data.data;
              }
  
        //  getErrorCodes();
        
      });

$http({
  method: 'GET',
  url: MSMARTEU.baseURL + "/getAllConsumerByJobIdAndAgencyID/agency/" + localStorageService.get("agency_id") + "/job_id/" + localStorageService.get("jobId"),
  headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": localStorageService.get("token"),"userId" : localStorageService.get("user_id") }
})
.then(function(response) {
  console.log(response.data);
  $scope.consList = response.data.data;
  $scope.Consumers.push(response.data.data);
  console.log($scope.Consumers)
  $scope.remainingItems = response.data.ramining;

  if($scope.remainingItems < 0){
    $scope.noMoreItemsAvailable = false;
  }
  $ionicLoading.hide().then(function() {
    console.log("The loading indicator is now hidden");
  });
})
function InsertErrorCodesOffline()
{
 for(var i = 0;i< $scope.errorCodes.length;i++)
 {
   var query = "INSERT INTO errorcodes(id,code,description) VALUES (?,?,?)";
   console.log($scope.errorCodes[i].id)
   console.log($scope.errorCodes[i].code)
   $cordovaSQLite.execute(db, query, [$scope.errorCodes[i].id,
     $scope.errorCodes[i].code,
     $scope.errorCodes[i].description
     ])
   .then(function(res) {
    console.log("insertId: " + res.insertId);
    console.log(res.rows.length)
  }, function (err) {
    console.error(err);
  });
 }
}

$scope.showConsumers = function()
{
  console.log("Inside Show Consumers")
  console.log($stateParams.jobId)
  $scope.jobId = $stateParams.jobId;
  InsertErrorCodesOffline();
  $state.go('app.CustomerList',{'jobId': $scope.jobId});
}
$scope.showMeterReadings = function()
{
  console.log("in the function")
  $state.go('app.meterReadingDetails',{'jobId': $scope.jobId})
}
$scope.isDisabled = false;
$scope.sync = function()
{
  InsertErrorCodesOffline();
  var flag =0;
  $scope.isDisabled = true;
  $ionicLoading.show({
    template: 'Inserting Consumers Into Local DataBase..'
  }).then(function() {
    console.log("The loading indicator is now displayed");
  });
  $scope.consumerLength = $scope.consList.length;

  for(var i = 0;i< $scope.consList.length;i++)
  {
       var query = "INSERT INTO customer(consumer_account_number,username,job_id,job_detail_id,job_status,meter_reading,meter_reader_id,meter_reading_status,meter_reading_taken,mobile_number,reading_taken_on,route_no,status,user_id,average_consumption,previous_reading,connection_type,previous_error_code_id,error_code_description,connection_description) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $cordovaSQLite.execute(db, query, [$scope.consList[i].consumer_account_number,
         $scope.consList[i].username,
         $scope.consList[i].job_id,
         $scope.consList[i].job_detail_id,
         $scope.consList[i].job_status,
         $scope.consList[i].meter_reading,
         $scope.consList[i].meter_reader_id,
         $scope.consList[i].meter_reading_status,
         $scope.consList[i].meter_reading_taken,
         $scope.consList[i].mobile_number,
         $scope.consList[i].reading_taken_on,
         $scope.consList[i].route_no,
         $scope.consList[i].status,
         $scope.consList[i].user_id,
         $scope.consList[i].average_consumption,
         $scope.consList[i].previous_reading,
         $scope.consList[i].connection_type,
         $scope.consList[i].previous_error_code_id,
         $scope.consList[i].error_code_description,
         $scope.consList[i].connection_description
         ])
       .then(function(res) {
        if(res.rowsAffected == 1)
        {
          flag++;
        }
        console.log(flag)
        console.log("insertId: " + res.insertId);
        console.log(res.rows.length)
        console.log(i)
         if( flag == $scope.consList.length)
        {
          console.log(i)
          console.log($scope.consList.length)
          $ionicLoading.hide().then(function() {
            console.log("The loading indicator is now hidden");
          }); 
        }

      }, function (err) {
        console.error(err);
      //console.log(JSON.stringify(err))
      console.log(err.toString())
    });
     }

   };

   $scope.view = function(jobId)
   {
     console.log($scope.jobId)
     console.log("Inside View Consumers")
     console.log($stateParams.jobId)
     $scope.jobId = $stateParams.jobId;
     showDetails();
     $state.go('app.CustomerList',{'jobId': $scope.jobId});
     console.log($scope.allData)
   }

   function showDetails()
   {
   	console.log($scope.jobId)
   	console.log($stateParams.jobId)
    var query = "select * FROM customer WHERE job_id = ?";
    console.log(query)
    $cordovaSQLite.execute(db, query,[$scope.jobId]).then(function(res) {
      console.log("insertId: " + res.insertId);
      console.log(res.rows.length)

      if(res.rows.length)
      {
        for(var i=0;i<res.rows.length;i++)
        {
          $scope.allData.push(res.rows.item(i));
        }
        console.log($scope.allData.length)
        console.log($scope.allData)
      }
      else{
        console.log("Data not there")
      }
      console.log($scope.allData)
    }, function (err) {
      console.error(err);
    });
  }
  console.log("In ConsList Ctrl");
  console.log($scope.allData)
  console.log($scope.allData.length)

  $ionicPlatform.registerBackButtonAction(function (event) {
    if($state.current.name == 'app.syncData'){
    $state.go('app.OpenJobsList')
    }
    else 
    { 
     navigator.app.backHistory();
    }
  }, 100);
});