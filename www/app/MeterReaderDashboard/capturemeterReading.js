    app.controller('meterReaderCtrl', function($cordovaNetwork,$state,$cordovaSQLite, $ionicPopup, $timeout, $scope, $http, $ionicPlatform, $stateParams, $cordovaCamera, $cordovaToast, $cordovaActionSheet, $cordovaFileOpener2, $cordovaFile, $cordovaImagePicker, $stateParams, $cordovaLocalNotification, $filter, localStorageService, $timeout, $cordovaFileTransfer,azureBlob) {
      console.log("MeterReading Ctrl")
      $scope.formData = {};
      $scope.formData.status = '';
      $scope.formData.reading = '';
      $scope.formData.mobile_no = '';
      $scope.formData.email_id = '';
      $scope.formData.route_no = '';
      $scope.customerId = '';
      $scope.customerName = '';
      $scope.error_code = '';
      $scope.jobId = '';
      $scope.jobDetailId = '';
      $scope.userData = {};
      $scope.imageUrl = '';
      $scope.flag_updateConnection = '';
      var email_id = '';
      var mobile_no = '';
      var exp = '';
      var prevReading = '';
      var prev_error_code = '';
      $scope.doubleclikstatus = false;
      console.log($stateParams.consumerId)
      console.log($stateParams.consumerName)
      console.log($stateParams.jobId)
      console.log($stateParams.jobDetailId)
      if ($stateParams.consumerId) {
        $scope.customerId = $stateParams.consumerId
        $scope.consumerName = $stateParams.consumerName
        $scope.jobId = $stateParams.jobId
        $scope.jobDetailId = $stateParams.jobDetailId
      }
      $scope.pictureUrl = 'http://placehold.it/150x150';

      getOfflineData();
      
      console.log($scope.disabled)
      console.log($scope.customerId)
      console.log("COnsumer Details")

        function getOfflineData()
        {
          $scope.Consumers = [];
       var query = "select * FROM customer WHERE consumer_account_number = ?";
       console.log(query)
       $cordovaSQLite.execute(db, query,[$scope.customerId]).then(function(res) {
      console.log("insertId: " + res.insertId);
      console.log(res.rows.length)

      if(res.rows.length)
      {
        for(var i=0;i<res.rows.length;i++)
        {
          $scope.Consumers.push(res.rows.item(i));
        }
       // console.log($scope.Consumers)
       // console.log($scope.Consumers[0].route_no)
       OnServerResponse();
         }
     else{
        console.log("Data not there")
      }
       //console.log($scope.allData)
    }, function (err) {
      console.error(err);
    });

       $scope.errorCodes = [];
       var query = "select * FROM errorcodes";
         $cordovaSQLite.execute(db, query).then(function(res) {
      console.log("insertId: " + res.insertId);
      console.log(res.rows.length)
      if(res.rows.length)
      {
        for(var i=0;i<res.rows.length;i++)
        {
          $scope.errorCodes.push(res.rows.item(i));
        }
       console.log($scope.errorCodes)
         }
     else{
        console.log("Data not there")
      }
       //console.log($scope.allData)
    }, function (err) { 
      console.error(err);
    });
 }
           

      function OnServerResponse() {
        $scope.formData.route_no = $scope.Consumers[0].route_no;
        $scope.dbFunc = function() {
          console.log($scope.doubleclikstatus)
          // alert("doble click")
          $scope.doubleclikstatus = true;
          $scope.flag_updateRouteNo = 'yes';
          console.log($scope.doubleclikstatus)
          $scope.route_no = $scope.formData.route_no;
        }

        $scope.updateReading = function() {
          $scope.route_no = $scope.Consumers[0].route_no;
          if ($scope.route_no == $scope.formData.route_no) {
            $scope.flag_updateRouteNo = 'no';
          }
          mobile_no = $scope.formData.mobile_no;
          email_id = $scope.Consumers[0].email_id;
          if (mobile_no == '' && $scope.formData.mobile_no == '') {
            $scope.flag_updateDetails = 'no';
            mobile_no = $scope.formData.mobile_no;
            email_id = $scope.Consumers[0].email_id;
          } else {
            $scope.flag_updateDetails = 'yes';
            mobile_no = $scope.formData.mobile_no;
            email_id = $scope.Consumers[0].email_id;
          }
          console.log(angular.toJson($scope.userData))
          console.log(exp)
          if ($scope.formData.status == '1' || $scope.formData.status == '7') {
            $scope.imageUrl = exp;
            $scope.Connection_Type = $scope.Consumers[0].connection_type_id;
            $scope.flag_updateConnection = 'no';
          } else if ($scope.formData.status == '9') {
            $scope.imageUrl = exp;
            $scope.Connection_Type = $scope.Consumers[0].connection_type_id;
            $scope.flag_updateConnection = 'no';
            if ($scope.formData.type == 'Domestic') {
              $scope.flag_updateConnection = 'yes';
              $scope.Connection_Type = '1';
            } else if ($scope.formData.type == 'Non - Domestic') {
              $scope.flag_updateConnection = 'yes';
              $scope.Connection_Type = '2';
            } else if ($scope.formData.type == 'Institutional') {
              $scope.flag_updateConnection = 'yes';
              $scope.Connection_Type = '3';
            }
          } else if ($scope.formData.status != '1' || $scope.formData.status != '9' || $scope.formData.status != '7') {
            $scope.imageUrl = exp;
            $scope.formData.reading = "0";
            $scope.Connection_Type = $scope.Consumers[0].connection_type_id;
            $scope.flag_updateConnection = 'no';
          }
          prevReading = $scope.Consumers[0].previous_reading;
          if($scope.Consumers[0].previous_error_code_id == null)
          {
            prev_error_code = prev_error_code;
          }
          else
          {
            prev_error_code = $scope.Consumers[0].previous_error_code_id;
          }
          

          $scope.userData = {
            "meter_reading_status": $scope.formData.status,
            "meter_reading": $scope.formData.reading,
            "uom": "KL",
            "reading_taken_on": $scope.date_now,
            "meter_reader_id": localStorageService.get("user_id"),
            "error_code": $scope.formData.status,
            "url": $scope.imageUrl,
            "created_by": localStorageService.get("username"),
            "job_detail_id": $scope.jobDetailId,
            "job_id": $scope.jobId,
            "consumer_account_number": $stateParams.consumerId,
            "update_contact_details": $scope.flag_updateDetails,
            "mobile_number": mobile_no,
            "email_id": email_id,
            "update_route_no": $scope.flag_updateRouteNo,
            "route_no": $scope.formData.route_no,
            "update_connection_type": $scope.flag_updateConnection,
            "new_connection_type": $scope.Connection_Type,
            "previous_reading": prevReading.toString(),
            "previous_error_code_id": prev_error_code.toString()
          };

     
          console.log("user Data")
          console.log(angular.toJson($scope.userData))
          console.log($scope.formData.reading - $scope.Consumers[0].previous_reading)
          console.log(2 * ($scope.Consumers[0].average_consumption))
          if (($scope.formData.reading - $scope.Consumers[0].previous_reading) > 2 * ($scope.Consumers[0].average_consumption)) {
            console.log("In the Loop")
          }
          if (exp == '') {
            alert("Must Upload An Image")
          } else if ($scope.formData.status == '1' || $scope.formData.status == '7' || $scope.formData.status == '9') {
            if (!$scope.formData.reading) {
              alert("Meter Reading Cannot Be 0 or Empty")
            } else if ($scope.formData.reading && ($scope.formData.reading < $scope.Consumers[0].previous_reading)) {
              var confirmPopup = $ionicPopup.confirm({
                title: 'Alert',
                template: 'Current Reading Cannot Be Less Than Previous Reading, Still Update?'
              });
              confirmPopup.then(function(res) {
                if (res) {
                  console.log('You are sure');
                  //postData();
                  insertIntoLocal();
                 
                } else {
                  console.log('You are not sure');
                }
              });

            } else if ($scope.formData.reading && (($scope.formData.reading - $scope.Consumers[0].previous_reading) > 2 * ($scope.Consumers[0].average_consumption))) {
              //alert("Consumption Cannot Be More Than Twice Of Average Consumption")
            var confirmPopup = $ionicPopup.confirm({
                title: 'Alert',
                template: 'Consumption Cannot Be More Than Twice Of Average Consumption, Still Update?'
              });
              confirmPopup.then(function(res) {
                if (res) {
                  console.log('You are sure');
                  insertIntoLocal();
                } else {
                  console.log('You are not sure');
                }
              });
            } else {
              insertIntoLocal();
            }
          } else {
            console.log("in the else block while Posting");
           insertIntoLocal();
          } // else end of the conditions
        } //update End 
      } // Server Resonpse End 
      function postData() {
        $http({
            method: 'POST',
            url: MSMARTEU.baseURL + "/createMeterReading",
            headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": localStorageService.get("token"),"userId" : localStorageService.get("user_id") },
            data: $scope.userData
          })
          .then(function(sample) { //Success callback
              console.log("why is not printing")
              console.log(sample)
              console.log(sample.data)
               if(sample.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
              else if (sample.data.success == true) {
                console.log("Sam console")
                $cordovaToast.showShortBottom("Meter Reading Updated Successfully")
                $state.go('app.CustomerList', { 'jobId': $scope.jobId }, { reload: true })
              }
            },
            function(sample) { //error call back
              //Second function handles error
              console.log("Not posting Meter Reading")
              //alert("Cannot update Meter Reading")
            });
       }
       function insertIntoLocal()
      {
        var query = "UPDATE customer SET meter_reading_taken='yes' WHERE consumer_account_number = ?";
        $cordovaSQLite.execute(db, query,[$scope.customerId]).then(function(res) {
         }, function (err) {
        console.error(err);
       });
        console.log($scope.formData.status)
        var query = "INSERT INTO meterreading(consumer_account_number,meter_reading_status,meter_reading,error_code,uom,reading_taken_on,meter_reader_id,url,created_by,job_detail_id,job_id,update_contact_details,mobile_number,email_id,update_route_no,route_no,update_connection_type,new_connection_type,previous_reading,previous_error_code_id,update_to_server,meter_reading_taken) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          $cordovaSQLite.execute(db, query, [$stateParams.consumerId,
          $scope.formData.status,$scope.formData.reading,
          $scope.formData.status,"KL",
          $scope.date_now,localStorageService.get("user_id"),$scope.imageUrl,
          localStorageService.get("username"),$scope.jobDetailId,$scope.jobId,
          $scope.flag_updateDetails,mobile_no,email_id,
          $scope.flag_updateRouteNo,$scope.formData.route_no,$scope.flag_updateConnection,
          $scope.Connection_Type,prevReading.toString(),prev_error_code.toString(),"no","yes"])
        .then(function(res) {
          console.log("insertId: " + res.insertId);
          console.log(res.rows.length)
           $cordovaToast.showShortBottom("Meter Reading Updated Successfully")
                  $state.go('app.CustomerList', { 'jobId': $scope.jobId }, { reload: true })
        }, function (err) {
          console.error(err);
        });
      }
      $scope.img = []
      $scope.fileSets = []
      $scope.fileCount = 0
      console.log("Meter Reading")
      console.log($scope.formData.reading)
      $scope.takePhoto = function() {
         if ($scope.fileCount < 3) {
          document.addEventListener("deviceready", function() {
            var options = {
              quality: 50,
              destinationType: Camera.DestinationType.FILE_URI,
              sourceType: Camera.PictureSourceType.CAMERA,
              // allowEdit: true,
              encodingType: Camera.EncodingType.PNG,
              targetWidth: 600,
              targetHeight: 600,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false,
              correctOrientation: false
            };

            $cordovaCamera.getPicture(options)
              .then(function(imageData) {
                $scope.pictureUrl = imageData;
                console.log("Camera Daaaa" + angular.toJson(imageData))
                console.log(imageData)
                 var sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      var sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);

      console.log("Copying from : " + sourceDirectory + sourceFileName);
      console.log("Copying to : " + cordova.file.externalDataDirectory + sourceFileName);
      $cordovaFile.copyFile(sourceDirectory, sourceFileName, cordova.file.externalDataDirectory, $scope.customerId+"_"+$scope.date_now+".png").then(function(success) {
         $scope.fileName = cordova.file.externalDataDirectory + $scope.customerId+"_"+$scope.date_now+".png";
         console.log($scope.fileName);
         exp = $scope.fileName;
         console.log(exp)
         console.log(success);
      }, function(error) {
         console.dir(error);
      });
                //     this.pictureUrl = imageData;
                //     console.log(this.pictureUrl)

                //     var serverUrl  = "http://13.229.52.212:3001/file-upload";
                //     var fileURL = $scope.pictureUrl;
                //      console.log($scope.pictureUrl);

                //    var options = {};
                //  console.log("hey Ya Am I printing")
                // $cordovaFileTransfer.upload(serverUrl, fileURL, options)
                // .then(function(result) {
                //   // Success!
                //  // console.log(result)
                //   console.log(result.response)

                //   console.log(JSON.parse(result.response))
                //   var obj = JSON.parse(result.response)
                //    exp = obj["data"][0];
                //   console.log(exp);
                //   console.log($scope.imageUrl)
                // }, function(err) {
                //   // Error
                //   console.log(err)
                // }, function (progress) {
                //   // constant progress updates
                //   console.log(progress)
                // });
                $cordovaFile.checkFile(imageData, '')
                  .then(function(entry) {
                    // success
                    console.log(entry)
                    console.log(entry.start)
                    console.log($stateParams.consumerId)
                    console.log($scope.date_now)
                    // var name = entry.name;
                    var name = $stateParams.consumerId + '_' + $scope.date_now + '.png';
                    console.log(name)
                    console.log(entry.name)
                    console.log(name)
                    exp = "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
                    console.log(exp)

                    if($cordovaNetwork.isOnline() == true)
                     {
                       // blobUpload(entry);
                       uplodaImageOffline(imageData);
                     }
                     else if($cordovaNetwork.isOnline() == false)
                     {
                        uplodaImageOffline(imageData);
                     }
                    
                  }, function(error) {               
                    console.log(error)
                  });
                //$cordovaToast.showShortBottom("Photo Uploaded Successfully")
              }, function(err) {
                // error
                console.log("camera data:" + angular.toJson(imageData))
              });
          }, false);
        } else {
          $cordovaToast.showShortTop("You cannot attach more than 3 Documents")
        }
      }
      function blobUpload(entry)
      {
        console.log(name)
        console.log(entry.name)
        console.log(exp)
        //console.log(data)

        entry.file(function(data) {
          console.log(data)
          var config = {
            // baseUrl: "https://msmarteublob.blob.core.windows.net/meterreading/" + name, // baseUrl for blob file uri (i.e. http://<accountName>.blob.core.windows.net/<container>/<blobname>),
            baseUrl : exp,
            sasToken: "?sv=2017-04-17&ss=b&srt=sco&sp=rwdlac&se=2020-12-31T01:57:45Z&st=2017-12-03T17:57:45Z&spr=https&sig=833najuJP0JinN%2FvB12KrZmoJl8NcA7Bu2pkB0cpVGw%3D", // Shared access signature querystring key/value prefixed with ?,
            file: data, // File object using the HTML5 File API,
            progress: function cb(file, data) {
              console.log(file)
              exp = "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
              console.log(exp)
              console.log(data)
            },
            // progress callback function,
            complete: function cb(file, data) {
              console.log(data)
              console.log("https://msmarteublob.blob.core.windows.net/meterreading/" + name)
              console.log(name)
            }, // complete callback function,
            error: function cb(error) {
            console.log(error)
              // exp = "";
              console.log(data)
              //console.log(file)
              console.log(exp)
              alert("Error While Uploading An Image, Check the SAS token for Blob")
            },
            // error callback function,
            blockSize: 1024 // Use this to override the DefaultBlockSize,
          }
          azureBlob.upload(config)
        })
      }
      function uplodaImageOffline(imageData)
      {
        console.log(exp)
        exp = angular.toJson(imageData);
        exp = imageData;
        console.log(exp)
      }
      $scope.date_now = $filter('date')(Date.now(), 'yyyy-MM-dd');
      //console.log("current Date");
   });