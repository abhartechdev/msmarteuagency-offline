app.controller('jobDetailsCtrl',function($state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,localStorageService){
 
 console.log("Job DetailsCtrl")
 console.log($stateParams.jobId)
 $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getOneJob/agency/"+localStorageService.get("agency_id")+"/job/"+$stateParams.jobId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })
.then(function(response) {
          console.log(response.data);
          if(response.status == 401){
              alert("Unauthorized Access")
              localStorageService.clear();
              $ionicHistory.clearHistory();
              $ionicHistory.clearCache().then(function(){
              $state.go('UserLogin', { reload: true })
              })
              }
              else if(response.data.success == true)
              {
              	$scope.jobData = response.data.data;
              }
          
          });


})
