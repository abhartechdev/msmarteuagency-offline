  app.controller('meterCtrl', function($filter,$state,$cordovaFileTransfer, $cordovaFile, $cordovaFileOpener2,azureBlob, $ionicPopup, $timeout, $scope, $http, $ionicPlatform, $stateParams, localStorageService, $ionicLoading,$cordovaSQLite,$cordovaToast,ConnectivityMonitor,$cordovaNetwork, $cordovaFile) {
  console.log($stateParams.jobId)
   $scope.date_now = $filter('date')(Date.now(), 'yyyy-MM-dd');
  $scope.jobId = $stateParams.jobId;
  $scope.x = [];
  var count = 0;
  $scope.countUpload = 0;
   $scope.progressPercent = 0;
   $scope.uploadingImages = false;
   $scope.exportData = function()
   {
    console.log("inside Export Data")
    $scope.jobData = [];
    var query = "select * FROM meterreading WHERE job_id = ?";
      $cordovaSQLite.execute(db, query,[$stateParams.jobId]).then(function(res) {
        console.log("insertId: " + res.insertId);
        console.log(res.rows.length)
        if(res.rows.length)
        {
         for(var i=0;i<res.rows.length;i++)
                    {
                     $scope.jobData.push(res.rows.item(i));
                    }
                   console.log($scope.jobData.length)
                   console.log($scope.jobData)
                    $cordovaFile.createFile(cordova.file.externalDataDirectory, "job_details"+$stateParams.jobId+".txt", true)
                  .then(function (success) {
                     // success
                     console.log(success)
                    // console.log($scope.allData)
                     var header = "consumer_account_number,meter_reading_status,meter_reading ,error_code ,uom ,reading_taken_on ,meter_reader_id ,url ,created_by ,job_detail_id ,job_id ,update_contact_details,mobile_number,email_id,update_route_no,route_no ,update_connection_type,new_connection_type ,previous_reading ,previous_error_code_id ,meter_reading_taken ,update_to_server";
                     var jsonObject = JSON.stringify($scope.jobData);
                     console.log(jsonObject);
                     var finalCSV = ConvertToCSV(jsonObject);
                     console.log(finalCSV);
                 $cordovaFile.writeFile(cordova.file.externalDataDirectory, "job_details"+$stateParams.jobId+".txt", $scope.jobData, true)
                  .then(function (success) {
                    // success
                    console.log(success)
                   $cordovaToast.showShortBottom("Exported Successfully To Text File")
                    console.log("check tHe files folder in data")
                  }, function (error) {
                  // error
                  });

                  $cordovaFile.writeFile(cordova.file.externalDataDirectory, "job_details"+$stateParams.jobId+".csv", header+'\r\n'+finalCSV, true)
                  .then(function (success) {
                    // success
                    console.log(success)
                   $cordovaToast.showShortBottom("Exported Successfully To Csv File")
                    console.log("check tHe files folder in data")
                  }, function (error) {
                  // error
                  console.log("error while exportung Csv")
                  });
                   }, function (error) {
                     // error
                   });
           }
       else{
          console.log("Data not there")
        }
      }, function (err) {
        console.error(err);
      });

   }
   var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId,'yes']).then(function(res) {
        console.log("insertId: " + res.insertId);
        console.log(res.rows.length)
         $scope.meterreadingtaken = res.rows.length;
        if(res.rows.length)
        {
         $scope.meterreadingtaken = res.rows.length;
        }
       else{
          console.log("Data not there")
        }
      }, function (err) {
        console.error(err);
      });

      var query = "select * FROM customer WHERE job_id = ? AND meter_reading_taken = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId,'no']).then(function(res) {
        console.log("insertId: " + res.insertId);
        console.log(res.rows.length)
        $scope.consumersRemaining= res.rows.length;
        if(res.rows.length)
        {
         $scope.consumersRemaining = res.rows.length;
        }
       else{
          console.log("Data not there")
        }
      }, function (err) {
        console.error(err);
      });

          var query = "select * FROM customer WHERE job_id = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId]).then(function(res) {
        console.log("insertId: " + res.insertId);
        console.log(res.rows.length)
        $scope.consumersAllocated = res.rows.length;
        if(res.rows.length)
        {
         $scope.consumersAllocated = res.rows.length;
        }
       else{
          console.log("Data not there")
        }
        console.log($scope.consumersAllocated)
      }, function (err) {
        console.error(err);
      });

         var query = "select * FROM meterreading WHERE job_id = ? AND update_to_server = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId,'yes']).then(function(res) {
       console.log("meter reading Updated to server")
        console.log(res.rows.length)
        $scope.updatedToServer = res.rows.length;
        if(res.rows.length)
        {
         console.log("meter reading")
         $scope.updatedToServer = res.rows.length;
        }
       else{
          console.log("Data not there")
        }
       // console.log($scope.consumersAllocated)
      }, function (err) {
        console.error(err);
      });
         

  $scope.DeleteJob = function()
  {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Clear The Job',
      template: 'Are you sure you want to Delete the Job from Local?'
    });
    confirmPopup.then(function(res) {
      if (res) {
    var query = "DELETE FROM meterreading WHERE job_id = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId]).then(function(res) {
        console.log("insertId: " + res.insertId);
       // alert("Deleted the job Successfully")
        console.log(res.rows.length)
       }, function (err) {
        console.error(err);
      });

         var query = "DELETE FROM customer WHERE job_id = ?";
         console.log(query)
         $cordovaSQLite.execute(db, query,[$stateParams.jobId]).then(function(res) {
        console.log("insertId: " + res.insertId);
       // alert("Deleted the job Successfully")
        console.log(res.rows.length)
       }, function (err) {
        console.error(err);
      });

         
         alert("Deleted the job Successfully")
         $state.reload();
       }
       else{
        console.log("you are not sure")
       }
     });
  }

  $scope.DeletePictures = function()
  {
    $cordovaFile.removeDir(cordova.file.externalDataDirectory, cordova.file.externalDataDirectory)
        .then(function (success) {
          // success
           console.log(cordova.file.externalDataDirectory);
          console("Remove Recursively Success")
        }, function (error) {
          // error
          console.log(cordova.file.externalDataDirectory);
          console.log(error)
          console.log("couldn't delete files")
        });
  }
  $scope.uploadToServer = function()
  {
    console.log($cordovaNetwork.isOnline())
    var confirmPopup = $ionicPopup.confirm({
      title: 'Upload Job To Server',
      template: 'Are you sure you want to upload the job to server?'
    });
    confirmPopup.then(function(res) {
      if (res) {
        console.log('You are sure');
         if($cordovaNetwork.isOnline() == true)
         {
           
          $ionicLoading.show({
          template: 'Upload In Progress, Please Dont Close The App<ion-spinner></ion-spinner>',
           animation: 'fade-in',
            noBackdrop: false,
            maxWidth: 200,
            showDelay: 500
        }).then(function() {
          console.log("The loading indicator is now displayed");
        });

        DataUploadToServer();
         }
         else if($cordovaNetwork.isOnline() == false)
         {
           alert("Please Switch on the Internet and try again")
         }
        function DataUploadToServer()
        {

          $scope.allData = [];
            var query = "select * FROM meterreading WHERE job_id = ? AND update_to_server =? AND meter_reading_taken = ?";
                console.log(query)
                $cordovaSQLite.execute(db, query,[$scope.jobId,'no','yes']).then(function(res) {
                  console.log(res.rows.length)
                  if(res.rows.length)
                  {
                    for(var i=0;i<res.rows.length;i++)
                    {
                     $scope.allData.push(res.rows.item(i));
                     $cordovaFile.createFile(cordova.file.externalDataDirectory, "meter_reading_details.txt", true)
                  .then(function (success) {
                     // success
                     console.log(success)
                     console.log($scope.allData)
                       $cordovaFile.writeFile(cordova.file.externalDataDirectory, "meter_reading_details.txt", $scope.allData, true)
                  .then(function (success) {
                    // success
                    console.log(success)
                    console.log("check tHe files folder in data")
                  }, function (error) {
                  // error
                  });
                  }, function (error) {
                     // error
                   });

                   }
                   console.log($scope.allData.length)
                   console.log($scope.allData)
                 }
                 else{
                  console.log("Data not there")
                  $ionicLoading.hide();
                  $scope.progressPercent = 0;
                  alert("No Data To Upload")
                }
                console.log($scope.allData)
               $scope.length = $scope.allData.length;

                for (var i = 0; i < $scope.allData.length; i++) {
                   $scope.iValue = i;
             ImageUploadToBlob(i);
               }
             }, function (err) {
               console.error(err);
             });
        } //ends DataUploadToServer Function
        
              } else {
                console.log('You are not sure');
              }
            });

  		           function postData(i)
                {
                  // $scope.x[i] = '';
                  console.log($scope.x[i])
                    $scope.userData = {
                "meter_reading_status": $scope.allData[i].meter_reading_status,
                "meter_reading": $scope.allData[i].meter_reading,
                "uom": "KL",
                "reading_taken_on": $scope.allData[i].reading_taken_on,
                "meter_reader_id": localStorageService.get("user_id"),
                "error_code": $scope.allData[i].error_code,
                "url": $scope.x[i],
                "created_by": localStorageService.get("username"),
                "job_detail_id": $scope.allData[i].job_detail_id,
                "job_id": $scope.allData[i].job_id,
                "consumer_account_number": $scope.allData[i].consumer_account_number,
                "update_contact_details": $scope.allData[i].update_contact_details,
                "mobile_number": $scope.allData[i].mobile_number,
                "email_id": $scope.allData[i].email_id,
                "update_route_no": $scope.allData[i].update_route_no,
                "route_no": $scope.allData[i].route_no,
                "update_connection_type": $scope.allData[i].update_connection_type,
                "new_connection_type": $scope.allData[i].new_connection_type,
                "previous_reading": $scope.allData[i].previous_reading,
                "previous_error_code_id": $scope.allData[i].previous_error_code_id
              };
              console.log("Consumer Data Uploading")
  	          console.log($scope.userData)

  	          $http({
              method: 'POST',
              url: MSMARTEU.baseURL + "/createMeterReading",
              headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==", "token": localStorageService.get("token"),"userId" : localStorageService.get("user_id") },
              data: $scope.userData
              })
              .then(function(sample) { //Success callback
                console.log(sample.data)
                if(sample.status == 401){
                alert("Unauthorized Access")
                localStorageService.clear();
                $ionicHistory.clearHistory();
                $ionicHistory.clearCache().then(function(){
                $state.go('UserLogin', { reload: true })
                })
                }
                else if (sample.data.success == true) {
                  
                  // console.log(i.toString())
                  localMeterReaderUpdate(i);
                 // console.log("Meter Reading Updated")  
                 }
              },
              function(sample) { //error call back
                //Second function handles error
                console.log("Not posting Meter Reading")
                alert("Cannot update Meter Reading")
              });
            }
            function ImageUploadToBlob(i)
                    {
                      console.log($scope.allData[i].url);
                      var imageData = $scope.allData[i].url;
                      $cordovaFile.checkFile(imageData, '')
                      .then(function(entry) {// success
                      var name = $scope.allData[i].consumer_account_number + '_' + $scope.date_now + '.png';
                      console.log(name)
                      exp = "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
                      console.log(exp)
                      console.log(MSMARTEU.baseUrl)
                      if(MSMARTEU.baseUrl == 'https://msmarteu.com/api')
                      {
                         blobUpload(entry,i,exp,name); //Blob Upload Images
                         //onlinefileUpload(imageData,i);
                      }
                      else
                      {
                        //onlinefileUpload(imageData,i); // file-Upload mProSmart Seerver
                      blobUpload(entry,i,exp,name);
                    }
                   
                     }, function(error) {  
                     //$ionicLoading.hide();             
                      console.log(error)
                    });
                  //$cordovaToast.showShortBottom("Photo Uploaded Successfully")
                }
         function localMeterReaderUpdate(i)
         {
              var query = "UPDATE meterreading SET update_to_server = 'yes' WHERE consumer_account_number = ?";
              $cordovaSQLite.execute(db, query,[$scope.allData[i].consumer_account_number]).then(function(res) {
              console.log("insertId: " + res.insertId);
              console.log(res.rows.length)
              console.log(res)
             // $scope.meterreadingtaken = res.rows.length;
              console.log("meter Reading Updated")
              console.log(res.rowsAffected)
              if(res.rowsAffected == 1)
              {
                $scope.countUpload++;
            
              $ionicLoading.show({
               scope: $scope,
           template: '<p>Data Uploading To Server For...</p><p>{{iValue}} of {{length}}</p><ion-spinner></ion-spinner>'
           }); 
                console.log("Uploadcount  "+$scope.countUpload)
                console.log("Metre Reading Updated for "+$scope.allData[i].consumer_account_number)
              }
              else if(res.rowsAffected == 2 || res.rowsAffected == 3){
          
                $scope.countUpload++;
                 $ionicLoading.show({
               scope: $scope,
           template: '<p>Data Uploading To Server For...</p><p>{{iValue}} of {{length}}</p><ion-spinner></ion-spinner>'
           }); 
               console.log("Uploadcount  "+$scope.countUpload)
               console.log("Metre Reading Updated for "+$scope.allData[i].consumer_account_number)
              }
              else{
              console.log("Data not there")
              }
            console.log($scope.allData.length)
            if(res.rowsAffected == ($scope.allData.length))
             {
             $state.reload();
             $ionicLoading.hide();
             alert("Data Has Been Uploaded Successfully")
            }
           else if($scope.countUpload == $scope.allData.length)
           {
            $state.reload();
            $ionicLoading.hide();
            alert("Data Has Been Uploaded Successfully")
           }
          }, function (err) {
            alert("Error While Uploading Data")
            console.error(err);
          });
         }
        function blobUpload(entry,i,exp,name)
        {
          console.log(exp)
          console.log(name)
          entry.file(function(data) {
            console.log(data)
            console.log(data.localURL)
            var config = {
              baseUrl: "https://msmarteublob.blob.core.windows.net/meterreading/" + name, // baseUrl for blob file uri (i.e. http://<accountName>.blob.core.windows.net/<container>/<blobname>),
              sasToken: "?sv=2017-04-17&ss=b&srt=sco&sp=rwdlac&se=2020-12-31T01:57:45Z&st=2017-12-03T17:57:45Z&spr=https&sig=833najuJP0JinN%2FvB12KrZmoJl8NcA7Bu2pkB0cpVGw%3D", // Shared access signature querystring key/value prefixed with ?,
              file: data, // File object using the HTML5 File API,
              progress: function cb(file, data) {
                console.log(file)
                exp = "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
                console.log(exp)
                console.log(data)
                 $ionicLoading.show({
               scope: $scope,
           template: '<p>Image Uploading...</p><p>{{iValue}} of {{length}}</p><ion-spinner></ion-spinner>'
           }); 

              },
              // progress callback function,
              complete: function cb(file, data) {
                console.log(data)
                console.log("https://msmarteublob.blob.core.windows.net/meterreading/" + name)
                $scope.x[i]= "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
                 console.log($scope.x[i]);
                  $ionicLoading.show({
               scope: $scope,
           template: '<p>Image Uploading...</p><p>{{iValue}} of {{length}}</p><ion-spinner></ion-spinner>'
           });
                 postData(i);
                console.log(name)
              }, // complete callback function,
              error: function cb(error) {
                console.error(error)
                console.log(error)
                $scope.x[i]= "https://msmarteublob.blob.core.windows.net/meterreading/" + name;
               // postData(i);
               // exp = "";
                alert("Error While Uploading An Image, Check the SAS token for Blob")
              },
              // error callback function,
              blockSize: 64 * 1024 * 1024 // Use this to override the DefaultBlockSize,
            }
            azureBlob.upload(config)
          })
          //postData(i);
        }

        function onlinefileUpload(imageData,i)
        {
             this.pictureUrl = imageData;
             console.log(imageData)
             console.log(this.pictureUrl)
             var serverUrl  = "http://13.229.52.212:3001/file-upload";
             var fileURL = this.pictureUrl;
             console.log(this.pictureUrl);
             var options = {};
             console.log("hey Ya Am I printing")
             $cordovaFileTransfer.upload(serverUrl, fileURL, options)
             .then(function(result) {
              console.log(result.response)
              console.log(JSON.parse(result.response))
              var obj = JSON.parse(result.response)
              $scope.x[i]= obj["data"][0];
              console.log($scope.x[i]);
              console.log($scope.imageUrl);
              postData(i);
              }, function(err) {
                      // Error
                       //$ionicLoading.hide();
                       $scope.imgUploadError = "ImageUpload is not done properly, upload Image Once again";
                       alert("Job Upload Has been stopped because of connection Issues, please Upload The Job Again")
                      console.log(err)
                    }, function (progress) {
                      // constant progress updates
                      console.log(progress)
                    });
          }
      }

       function ConvertToCSV(objArray) {
          var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
          var str = '';

          for (var i = 0; i < array.length; i++) {
              var line = '';
              for (var index in array[i]) {
                  if (line != '') line += ','

                  line += array[i][index];
              }

              str += line + '\r\n';
          }
             return str;
      }
  });