angular.module('starter.routes', [])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.dashboard',{
    url: '/dashboard',
    views: {
      'menuContent': {
        templateUrl : 'app/dashboard/dashboard.html',
        controller : 'dashboardCtrl'
      }
    }
  })

  .state('UserLogin',{
    url : '/UserLogin',
    templateUrl:'app/login/UserLogin.html',
    controller: 'loginController'
  })

  .state('app.profilesettings',{
    url : '/profilesettings',
    views: {
      'menuContent': {
        templateUrl:'app/profileSettings/profilesettings.html',
        controller: 'ProfileSettingsCtrl'
      }
    }
  })

  .state('forgotPassword',{
    url : '/forgotPassword',
    templateUrl:'templates/forgotPassword.html'
  })
  .state('PasswordLink',{
    url : '/PasswordLink',
    templateUrl:'templates/PasswordLink.html'
  })

  .state('app.CustomerDetails',{
    url: '/CustomerDetails/:billId',
    views: {
      'menuContent': {
        templateUrl : 'app/bill/CustomerDetails.html',
        controller : 'CustomerDetailsCtrl'
      },
      params: {
        billId: null,
      }
    }
  })

  .state('app.CustomerBillDetails',{
    url : '/CustomerBillDetails/:billId',
    views :{
      'menuContent': {
        templateUrl : 'app/bill/CustomerBillDetails.html',
        controller : 'DetailsCtrl'
      },
      params: {
        billId: null,
      }
    }
  })
  .state('app.AddReceiptDetails',{
    url : '/AddReceiptDetails/:billId',
    views :{
      'menuContent': {
        templateUrl : 'app/bill/AddReceiptDetails.html',
        controller : 'DetailsCtrl'
      },
      params:{
        billId : null,
      }
    }
  })
  .state('app.ReceiptList',{
    url : '/ReceiptList',
    views :{
      'menuContent': {
        templateUrl : 'app/receipts/ReceiptList.html',
        controller : 'ReceiptCtrl'
      }
    }
  })
  .state('app.PaymentDetails',{
    url : '/PaymentDetails/:receiptId',
    views :{
      'menuContent': {
        templateUrl : 'app/receipts/PaymentDetails.html',
        controller : 'paymentCtrl'
      },
      params:{
        receiptId : null,
      }
    }
  })

  .state('app.EditReceiptDetails',{
    url : '/EditReceiptDetails/:receiptId',
    views :{
      'menuContent': {
        templateUrl : 'app/editReceipt/EditReceiptDetails.html',
        controller : 'ReceiptCtrl'
      },
      params:{
        receiptId : null,
      }
    }
  })

  .state('app.CreateOtherReceipts',{
    url:'/CreateOtherReceipts',
    views:{
      'menuContent':{
        templateUrl : 'app/createOtherReceipt/CreateOtherReceipts.html',
        controller : 'createReceiptCtrl'
      }
    }
  })
  .state('app.ReceiptCorrection',{
    url:'/ReceiptCorrection',
    views:{
      'menuContent':{
        templateUrl : 'templates/ReceiptCorrection.html',
        controller : 'ReceiptCtrl'
      },
      params:{
        receiptId : null,
      }
    }
  })
  .state('app.MRDashboard',{
    url : '/MRDashboard',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/MRDashboard.html',
        controller  : 'DashCtrl'
      }
    }
  })

  .state('app.CustomerList',{
    url : '/CustomerList/:jobId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/CustomerList.html',
        controller  : 'CustomerListCtrl'
      },
      params: {
        jobId: null,
      }
    }
  })
  .state('app.capturemeterReading',{
    url : '/capturemeterReading/:consumerId/:consumerName/:jobId/:jobDetailId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/capturemeterReading.html',
        controller  : 'meterReaderCtrl'
      },
      params: {
        consumerId: null,
        consumerName : null
      }
    }
  })
  .state('app.OpenJobsList',{
    url : '/OpenJobsList',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/OpenJobsList.html',
        controller  : 'jobCtrl'
      }
    }
  })
  .state('app.jobDetails',{
    url : '/jobDetails/:jobId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/jobDetails.html',
        controller  : 'jobDetailsCtrl'
      },
      params: {
        jobId: null,
      }
    }
  })
  .state('app.updatedetails',{
    url : '/updatedetails/:consumerId/:consumerName/:jobId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/updatedetails.html',
        controller : 'updateCtrl'
      },
      params: {
        consumerId: null,
      }
    }
  })

  .state('app.syncData',{
    url : '/syncData/:jobId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/SyncLocal.html',
        controller  : 'syncLocalCtrl'
      },
      params: {
        jobId: null,
      }
    }
  })
  .state('app.offlinesettings',{
    url : '/offlinesettings',
    views:{
      'menuContent' :{
        templateUrl : 'app/offlinesettings/offlinesettings.html',
        controller  : 'offlineCtrl'
      }
    }
  })
  .state('app.meterReadingDetails',{
    url : '/meterReadingDetails/:jobId',
    views:{
      'menuContent' :{
        templateUrl : 'app/MeterReaderDashboard/meterReadingDetails.html',
        controller  : 'meterCtrl'
      },
      params: {
        jobId: null,
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('UserLogin');
});