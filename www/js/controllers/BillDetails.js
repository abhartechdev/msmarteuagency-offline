app.controller('CustomerDetailsCtrl',function($state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope){
 
 //  console.log($stateParams.custId);
 $scope.searchBy = '$';
  $scope.query = {};
  
 console.log($rootScope.userDetails.agency_id)
  $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllConsumerBillDetails/agency/"+$rootScope.userDetails.agency_id,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })
.then(function(response) {
          console.log(response.data);
          $scope.BillData = response.data.data;
          $scope.billId = response.data.bill_detail_id;
        //  onClickButton();
      });

$scope.formData = {};
$scope.formData.customerId = "";
console.log($stateParams.billId)
  if ($stateParams.billId) {
    $scope.customerId = $stateParams.billId
  }
console.log($scope.customerId)
$scope.getBillDetails = function(customerId)
{

  console.log($scope.customerId)
  console.log($scope.formData.customerId)
 $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getOneConsumerBillDetails/agency/"+$rootScope.userDetails.agency_id+"/consumer/"+$scope.formData.customerId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"Content-Type" : "application/json"}
      })
.then(function(response) {
          console.log(response.data);
           if(response.data.success == true)
          {
          $scope.BillData = response.data.data;
          $scope.billId = response.data.bill_detail_id;
        //  onClickButton();
          }
        else if (response.data.success == false)
              {
                alert("Please Enter Correct Consumer Account Number")
              }
      });
}
    
    $scope.goToBillDetails = function()
    {
      $state.go('app.CustomerBillDetails');
    }

    $scope.goToAddDetails = function()
    {
      $state.go('app.AddReceiptDetails');
    }
    
  $scope.editEmail = false;

  $scope.goToEditDetails = function()
  {
     $scope.editEmail = true;
     $state.go('app.EditReceiptDetails');
  }

  $scope.goToPaymentDetails = function()
    {
      $state.go('app.PaymentDetails');
    }

    $scope.date_rdv = $filter('date')(Date.now(), 'yyyy-MM-dd');
});