app.controller('ProfileSettingsCtrl', function($scope, $state,$rootScope,$http) {
  console.log("ProfileSettingsCtrl")
  console.log($rootScope)
  console.log($rootScope.userDetails)
  $scope.userName = $rootScope.userDetails.userName;
  $scope.email = $rootScope.userDetails.email;
  $scope.agencyId = $rootScope.userDetails.agency_id;

  // $scope.changePwd = function(oPwd, newPwd, rPwd) {
  //   if (oPwd == $rootScope.userDetails.password) {
  //     if (newPwd == rPwd) {
  //       $cordovaToast.showShortTop('Password updated successfully')
  //     } else {
  //       $cordovaToast.showShortTop('Password did not match')
  //     }
  //   } else {
  //     $cordovaToast.showShortTop('Please enter your valid password')
  //   }
  // }
  $scope.changePwd = function(oPwd, newPwd, rPwd) {
     if (newPwd == rPwd) {
    
      $scope.userPwdData = {
      "password": newPwd,
      "created_by"   : "user 1"
    }
    console.log($scope.userPwdData)
      $http({
        method: 'PUT',
        url: MSMARTEU.baseURL + "/updateUserPassword/"+$rootScope.userDetails.user_id,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" ,"token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")},
        data: $scope.userPwdData
      })
     .then(function(response) {
          console.log(response.data);
          console.log(response.data.data)
          if(response.data.success == true)
          {
             alert('Password updated successfully')
          }
          });
        //alert('Password updated successfully')
        $state.go('UserLogin')
      } 
      else {
        alert('Password did not match')
      }
  }
});
