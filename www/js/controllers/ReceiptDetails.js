app.controller('ReceiptCtrl',function($state,$ionicPopup, $timeout,$scope,$http,$ionicPlatform,$stateParams,$filter,$rootScope){
$scope.searchBy = '$'
$scope.query = {}
$scope.receiptData = {};
$scope.receiptData.remarks="";
$scope.receiptData.amountPaid = "";
$scope.formData = {};
console.log("Receipt History")
console.log($stateParams.receiptId)
console.log($rootScope.userDetails.agency_id)

$scope.getReceiptDetails = function(){
 $http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllReceipt/consumer/"+$scope.formData.consumerId,
        //+$stateParams.custId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==","token" : localStorageService.get("token"),"userId" : localStorageService.get("user_id")}
      })

.then(function(response) {

         console.log("Receipt Details")

          console.log(response.data);
         
          if(response.data.success == true)
          {
            console.log(response.data.data);
             $scope.ReceiptData = response.data.data;
          }
          else if (response.data.success == false)
              {
                alert("Please Enter Correct Customer Account Number")
              }
      
          });
}

$http({
        method: 'GET',
        url: MSMARTEU.baseURL + "/getAllReceipts/agency/"+$rootScope.userDetails.agency_id,
        //+$stateParams.custId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" }
      })

.then(function(response) {

         console.log("Receipt Details")

          console.log(response.data);
         
          if(response.data.success == true)
          {
            console.log(response.data.data);
             $scope.ReceiptData = response.data.data;
          }
          else if (response.data.success == false)
              {
                alert("Please Enter Correct Customer Account Number")
              }
      
          });
$scope.goToPaymentDetails = function()
    {
      
     $state.go('app.PaymentDetails');
    }

 $scope.date_rdv = $filter('date')(Date.now(), 'yyyy-MM-dd');
$scope.goToAddDetails = function()
    {
      $state.go('app.AddReceiptDetails');
    }
 $scope.goToEditDetails = function()
  {
     $scope.editEmail = true;
     $state.go('app.EditReceiptDetails');
  }
//  $http({
//         method: 'GET',
//         url: MSMARTEU.baseURL + "/getreceipt/"+$stateParams.receiptId,
//         //+$stateParams.custId,
//         headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" }
//       })
// .then(function(response) {

//          console.log("Receipt Details")

//           console.log(response.data);
         
//           if(response.data.success == true)
//           {
//             console.log(response.data.data);
//              $scope.ReceiptDetails = response.data.data;
//           }
      
//           });
  
$scope.saveReceiptDetails = function(data)
{
  $scope.ReceiptDetails = {
    "total_amount" : data.total_amount,
    "amount_paid" : $scope.receiptData.amountPaid,
    "balance_amount" : data.balance_amount,
    "payment_status" : data.payment_status,
     "remarks" : $scope.receiptData.remarks,
     "created_by" : data.created_by
  }

  console.log($scope.ReceiptDetails)
  console.log(data.balance_amount)
  if(!$scope.receiptData.remarks)
  {
    alert("The Remarks cannot be empty");
  }
  else if(data.balance_amount > $scope.receiptData.amountPaid)
  {
    alert("you cannot pay more than your balance Amount")
  }
else if(scope.receiptData.amountPaid && data.balance_amount == 0)
{
  alert("You Don't have to pay any money")
}
else
{
  $http({
        method: 'PUT',
        url: MSMARTEU.baseURL + "/updateReceipt/"+$stateParams.receiptId,
        headers: { "ab_authorization": "QUNNJmFiaGFydGVjaA==" },
        data: $scope.ReceiptDetails,
      })

     .then(function(response) {
          console.log(response)
          if(response.data.success == true) {
            console.log("Sam console")
            $state.go('app.ReceiptList')
          }
        });
}
}
});