var MSMARTEU = {
  // "baseURL": "http://52.172.25.44:3500",  // azure
  baseURL: "http://54.251.163.153:3500", // AWS Dev
  //"baseURL": "http://54.169.114.99:3500", // AWS Demo/ UAT
  //"baseURL": "https://msmarteu.com/api", //Production
  // "baseURL" : "http://192.168.1.106:3500", // local
  // "baseURL" : "http://localhost:3500", //LocalHost
  createuser: "/createUser",
  validateUser: "/validateUser",
  createAccount: "/createAccount",
  validateAgencyUser: "/validateAgencyUser",
  createReceipt: "/createReceipt",
  createReceiptByMeterReader: "/meterReader/createReceipt"
};
