// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.routes','starter.services','ng-mfb','LocalStorageModule','ngCordova','starter.directives','azureBlobUpload','ngSanitize','ngCsv'])
//var app = angular.module('your-app', ['ng-mfb']);

.run(function($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
     }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    } 
    db = $cordovaSQLite.openDB({name: "my.db",iosDatabaseLocation:'default'});
   //$cordovaSQLite.execute(db,"DROP TABLE IF EXISTS errorcodes");
   //$cordovaSQLite.execute(db,"DROP TABLE IF EXISTS customer");
    //$cordovaSQLite.execute(db,"DROP TABLE IF EXISTS meterreading");
   $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS customer (id integer primary key, consumer_account_number text, username text,job_id integer,job_detail_id integer,job_status text,meter_reading text,meter_reader_id text,meter_reading_status text,meter_reading_taken text,mobile_number text,reading_taken_on text,route_no text,status text,user_id text,average_consumption text,previous_reading text,connection_type text,previous_error_code_id text,error_code_description text,connection_description text, error_code text)");
   $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS errorcodes (id integer unique, code text , description text)")
   $cordovaSQLite.execute(db,"CREATE TABLE IF NOT EXISTS meterreading (consumer_account_number integer,meter_reading_status text,meter_reading text,error_code text,uom text,reading_taken_on text,meter_reader_id text,url text,created_by text,job_detail_id text,job_id text,update_contact_details text,mobile_number text,email_id text,update_route_no text,route_no text,update_connection_type text,new_connection_type text,previous_reading text,previous_error_code_id text,meter_reading_taken text,update_to_server text)") 
  });
})


